import Vector from './modules/Vector.js'
import Light from './modules/Light.js'
import Sphere from './modules/Sphere.js'
import Ray from './modules/Ray.js'

function test() {
	let v1 = new Vector(3.4367895, 4.345965423, 5.895445)
	let v2 = new Vector(8, 8, 8)

	console.log('v1', v1)
	console.log('v2', v2)

	console.log('v1.add(v2)', v1.add(v2))
	console.log('v1.subtract(v2)', v1.subtract(v2))
	console.log('v1.dot(v2)', v1.dot(v2))
	console.log('v1.scale(3)', v1.scale(3))
	console.log('v1.magnitude', v1.magnitude)
	
	console.log('v1.normalise()', v1.normalise())
	console.log('v1.normalise().magnitude', v1.normalise().magnitude)
	
	console.log('v1.negate()', v1.negate())

	let light1 = new Light(new Vector(20,  30, 30), 2,   1.5)
	let light2 = new Light(new Vector(-20, 30, 30), 1.5, 2  )

	console.log('light1', light1)
	console.log('light2', light2)

	let sphere1 = new Sphere(new Vector(0, 0, -18), 4)
	let intersect = sphere1.rayIntersect(
		new Ray(new Vector(0, 0, 0), new Vector(0, 0, -1).normalise())
	)
	console.log(intersect)

	console.log(Vector.interpolate(v1, v2, 0.75))
}

// Gamma test
/*
let pixel = new Colour(1, 1, 1).scale(col/50)

pixel.r = Math.floor(pixel.r)
pixel.g = Math.floor(pixel.g)
pixel.b = Math.floor(pixel.b)

pixel = pixel.scale(1/20)

if (row > frameBuffer.height / 3 && row < frameBuffer.height * 2 / 3)
	pixel = pixel.gammaCorrect()
else if (row >= frameBuffer.height * 2 / 3)
	pixel = pixel.gammaCorrect(2.2)

frameBuffer.setPixel(pixel, row, col)
*/