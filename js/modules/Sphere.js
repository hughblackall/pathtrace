import Ray from './Ray.js'
import RayIntersect from './RayIntersect.js'
import SceneObject from './SceneObject.js'
import Material from './Material.js'
import Vector from './Vector.js'

/** 
 * Represents a sphere in 3 dimensional Euclidean space
 * 
 * @extends SceneObject
 */
export default class Sphere extends SceneObject {
	/**
	 * Create a sphere
	 * 
	 * @param {Vector} position - The sphere position in 3 dimensional space
	 * @param {number} radius - The radius of the sphere
	 * @param {Material} material - The material the sphere is made of
	 */
	constructor(position, radius, material) {
		super(material)
		this.position = position
		this.radius = radius
		this.type = Sphere.type
	}

	static type = 'sphere'

	/**
	 * Check if a ray intersects with the sphere.
	 * 
	 * There are three possibilities: there is no intersection, 
	 * there is a single point of intersection (ray grazes the surface), or 
	 * there are two points of intersection (entry and exit).
	 * 
	 * @param {Ray} ray - The ray to check for intersection
	 * @return {RayIntersect}
	 */
	rayIntersect(ray) {

		// l is the vector from the source to the center of the sphere
		let l = this.position.subtract(ray.source)
    
		// a is the dot product of the ray direction and l and therefore is the
		// magnitude of the projection of l on the ray as the ray direction is a 
		// unit vector
		let a = l.dot(ray.direction)
		
		// d is the distance between the sphere center and the ray. d2 is the 
		// square of d, calculated using Pythagoras' theorem
		let d2 = l.dot(l) - a * a
		
		// If d is greater than the radius of the sphere the ray does not 
		// intersect
		if (d2 > this.radius * this.radius) return new RayIntersect(false)
		
		// b is the distance of the ray from the surface of the sphere
		let b = Math.sqrt(this.radius * this.radius - d2)
		
		// t0 is the distance from the source to the entry intersection of the 
		// ray with the sphere
		let t0 = a - b
		// t1 is the distance from the source to the exit intersection of the 
		// ray with the sphere
		let t1 = a + b
		
		// If t0 is less than 0 and t1 is greater, the source is inside the 
		// sphere and we'll say the ray doesn't intersect
		if (t0 < 0 && t1 > 0 ) return new RayIntersect(false)
		// If t1 is less than 0, the source is past the sphere and therefore the 
		// ray doesn't intersect
		if (t1 < 0) return new RayIntersect(false)
	
		// If none of the above checks pass, the ray intersects with the sphere
		return new RayIntersect(true, t0)
	}

	/** 
	 * Find the normal vector at a given location on the surface of the sphere
	 * 
	 * @override
	 * @param {Vector} location - The location on the sphere
	 * @param {Ray} ray - The ray with which the sphere intersects (not used for 
	 *     spheres)
	 * @return {Vector} - Normalised normal vector
	 */
	findNormal(location, ray) {
		return location.subtract(this.position).normalise()
	}

	static fromPlainJS(sphere) {
		return new Sphere(
			Vector.fromPlainJS(sphere.position),
			sphere.radius,
			Material.fromPlainJS(sphere.material)
		)
	}
}