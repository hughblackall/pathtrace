import Material from './Material.js'

/** Represents an object in 3 dimensional Euclidean space */
export default class SceneObject {
	/**
	 * Create a scene object
	 * 
	 * @param {Material} material - The material the object is made of
	 */
	constructor(material) {
		if (typeof this.rayIntersect !== 'function') {
			throw new TypeError(
				'Subclass of SceneObject must implement rayIntersect(ray: Ray): Vector'
			)
		}

		this.material = material
	}
}