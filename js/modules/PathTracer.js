import Scene from './Scene.js'
import FrameBuffer from './FrameBuffer.js'
import Camera from './Camera.js'
import WorkerPool from './WorkerPool.js'

/** Renders a scene using a path tracing algorithm */
export default class PathTracer {
	/**
	 * Create a path tracer
	 * 
	 * @param {Object} params
	 * @param {Scene} params.scene - The scene to render
	 * @param {Camera} params.camera - A camera to define the position from which 
	 *     to capture the scene
	 * @param {number} params.settings.maxDepth - The maximum number of ray 
	 *     reflections to perform per pixel
	 * @param {boolean} params.settings.bounceMasking - Whether to represent 
	 *     absorption of colours as rays bounce off objects in the scene
	 * @param {number} params.settings.threadCount - The number of web workers 
	 *     to spawn and use when rendering a frame
	 * @param {boolean} params.settings.allowSecondaryRoughness - Whether to 
	 *     to render secondary roughnesses for materials that have them
	 */
	constructor({ 
		scene,
		camera,
		settings: { 
			maxDepth,
			bounceMasking,
			threadCount,
			allowSecondaryRoughness
		},
	}) {
		this.scene = scene
		this.camera = camera
		this.settings = {
			maxDepth,
			bounceMasking,
			threadCount,
			allowSecondaryRoughness,
		}
		this.depthCount = []
		this.intersectCount = {}
		this.workerFrameBuffer = null
	}

	/**
	 * Renders multiple frames and averages them
	 * 
	 * @param {Object} params
	 * @param {number} params.frameLimit - The number of frames to render and 
	 *     average
	 * @param {function({
	 *         frameBuffer: FrameBuffer,
	 *         frameCount: number,
	 *         frameLimit: number
	 *     }): void} params.frameReadyCallback - A function that will be called 
	 *     after every frame is rendered with the accumulated framebuffer, 
	 *     number of frames rendered, and total number of frames to be rendered
	 * @returns {Promise<{frameBuffer: FrameBuffer, framesRendered: number}>} - 
	 *     The final rendered image combining all rendered frames and the total 
	 *     number of frames rendered
	 */
	render({frameLimit, frameReadyCallback}) {
		console.debug('render() called')

		return new Promise((resolve, reject) => {
			let frameCount = 0
			let accumulatedImage = new FrameBuffer(1024, 576)

			function renderLoop(timestamp) {

				this.renderFrame().then((frame) => {
					accumulatedImage = accumulatedImage.add(frame)
					frameCount++
					const currentResult =
						accumulatedImage.divideByConstant(frameCount)
				
					frameReadyCallback({
						frameBuffer: currentResult,
						frameCount,
						frameLimit
					})
				
					if (frameCount < frameLimit) {
						window.requestAnimationFrame(renderLoop.bind(this))
					} else {
						resolve({
							frameBuffer: currentResult,
							framesRendered: frameCount,
						})
					}
				})
			}

			window.requestAnimationFrame(renderLoop.bind(this))
		})
	}

	/** 
	 * Renders a frame of the scene
	 */
	renderFrame() {
		let framePromise = new Promise((frameResolve, frameReject) => {

			this.workerFrameBuffer = new FrameBuffer(1024, 576)

			const workerPool = new WorkerPool(
				this.settings.threadCount,
				this.rowComplete, this
			)

			workerPool.init(
				this.scene,
				this.camera,
				this.settings
			).then(() => {

				let rowPromises = []

				// Loop through each row in the frame and pass it to a worker 
				// pool to be rendered in parallel on a separate thread
				for (let row = 0; row < this.workerFrameBuffer.height; row++) {

					const promise = workerPool.addTask({
						row, 
						width: this.workerFrameBuffer.width,
						height: this.workerFrameBuffer.height,
					})
					rowPromises.push(promise)
				}

				// Once all rows have finished rendering, return the completed 
				// frame
				Promise.all(rowPromises).then(() => {
					console.debug('All row promises resolved')
					frameResolve(this.workerFrameBuffer)
					workerPool.terminate()
				})
			})

		})

		return framePromise
	}

	/**
	 * Passed to a worker pool to be called on completion of each row
	 */
	rowComplete(data) {
		this.workerFrameBuffer.insertRow(data.rowBuffer, data.row)
	}

	static fromPlainJS(pathTracer) {
		return new PathTracer({
			scene: Scene.fromPlainJS(pathTracer.scene),
			camera: Camera.fromPlainJS(pathTracer.camera),
			settings: pathTracer.settings
		})
	}
}