import Vector from './Vector.js'
import Colour from './Colour.js'
import Sphere from './Sphere.js'
import Material from './Material.js'

/** 
 * Represents an extended (non-point), spherical light source in 3 dimensional 
 * Euclidean space
 */
export default class Light extends Sphere {
	/**
	 * Create a light source
	 * 
	 * @extends Sphere
	 * @param {Vector} position - The light position in 3 dimensional space
	 * @param {number} radius - The radius of the light
	 * @param {number} intensity - The intensity of the light
	 * @param {Colour} colour - The colour of the light. RGB components can be 
	 *     greater than 1 to represent light emission as opposed to absorption.
	 */
	constructor(position, radius, intensity, colour) {
		super(position, radius)
		this.intensity = intensity
		this.material = new Material({ colour, roughness: null })
		this.type = Light.type
	}

	static type = 'light'

	/** Find a scaling factor for the apparent size of this light at the given 
	 * distance
	 * 
	 * The angular size of a sphere is given by 2*arcsin(radius/distance). This 
	 * value is squared here so that it scales with the apparent area of the 
	 * light, which is what we are interested in when dealing with apparent 
	 * intensity. Any constants in front, including dividing by the total area 
	 * of the sky, can be ignored here and rolled into the light intensity.
	 * 
	 * @param {number} distance - The distance from which the light is viewed
	 * @return {number}
	*/
	apparentSize(distance) {
		return Math.pow(Math.asin(this.radius / distance), 2)
	}

	static fromPlainJS(light) {
		return new Light(
			Vector.fromPlainJS(light.position),
			light.radius,
			light.intensity,
			Colour.fromPlainJS(light.material.colour)
		)
	}
}