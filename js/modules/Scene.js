import SceneIntersect from './SceneIntersect.js'
import Ray from './Ray.js'
import SceneObject from './SceneObject.js'
import Light from './Light.js'
import Colour from './Colour.js'
import Sphere from './Sphere.js'
import PlaneXZ from './PlaneXZ.js'
import Vector from './Vector.js'
import Material from './Material.js'

/** Represents a scene composed of spheres, planes and light sources */
export default class Scene {
	/**
	 * Create a scene
	 * 
	 * @param {[SceneObject]} sceneObjects - An array of objects and lights to 
	 *     display in the scene
	 * @param {Colour} ambientColour - The colour of ambient light in the scene 
	 * @param {number} ambientIntensity - The intensity of ambient light in the 
	 *     scene 
	 */
	constructor(sceneObjects, ambientColour, ambientIntensity) {
		this.sceneObjects = sceneObjects
		this.ambientColour = ambientColour
		this.ambientIntensity = ambientIntensity
	}

	/**
	 * Check if a ray intersects with any objects in the scene and return 
	 * details about the closest intersection
	 * 
	 * @param {Ray} ray - The ray to check for intersection
	 * @returns {SceneIntersect}
	 */
	rayIntersect(ray) {

		let result = new SceneIntersect()
		
		// Loop through each scene object
		this.sceneObjects.forEach((object) => {
			// Check if the ray intersects with the current object
			let rayIntersect = object.rayIntersect(ray)

			// If it does, and it's the closest intersect so far, update details 
			// to be returned
			if (rayIntersect.intersects 
			 && rayIntersect.distance < result.distance) {
				
				result.distance = rayIntersect.distance
				result.location = ray.source.add(
					ray.direction.scale(rayIntersect.distance)
				)
				result.normal = object.findNormal(result.location, ray)
				result.material = object.material
				if (object instanceof Light)
					result.light = true
				result.intersects = true
			}
		})

		return result
	}

	static fromPlainJS(scene) {
		let sceneObjects = []
		scene.sceneObjects.forEach((obj) => {
			switch (obj.type) {
				case Sphere.type:
					sceneObjects.push(new Sphere(
						Vector.fromPlainJS(obj.position),
						obj.radius,
						Material.fromPlainJS(obj.material)
					))
					break
				case PlaneXZ.type:
					sceneObjects.push(new PlaneXZ(
						obj.y,
						Material.fromPlainJS(obj.material)
					))
					break
				case Light.type:
					sceneObjects.push(new Light(
						Vector.fromPlainJS(obj.position),
						obj.radius,
						obj.intensity,
						Colour.fromPlainJS(obj.material.colour)
					))
					break
			}
		})

		return new Scene(
			sceneObjects,
			Colour.fromPlainJS(scene.ambientColour),
			scene.ambientIntensity
		)
	}
}