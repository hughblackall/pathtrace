import Vector from './Vector.js'
import SceneIntersect from './SceneIntersect.js'

/** Represents a ray in 3 dimensional Euclidean space */
export default class Ray {
	/**
	 * Create a ray
	 * 
	 * @param {Vector} source - The staring point of the ray in 3 dimensional 
	 *     space
	 * @param {Vector} direction - A normalised vector representing the direction 
	 *     the ray is travelling
	 */
	constructor(source, direction) {
		this.source = source
		this.direction = direction
	}

	/**
	 * Reflect this ray perfectly off a surface at the specified intersect. The 
	 * source of the new ray will be offset slightly in the normal direction to 
	 * avoid intersecting with the surface off which it is being reflected.
	 * 
	 * @param {SceneIntersect} intersect - The location on the surface at which  
	 *     this ray is to be reflected
	 * @return {Ray} 
	 */
	reflect(intersect) {
		// This ray serves as the incident ray whose direction vector we reflect
		const reflectionDirection = this.direction.reflect(intersect.normal)
		// Move the intersect location in the normal direction slightly so that  
		// the new ray doesn't intersect with the reflecting object
		const reflectionSource = intersect.getOffsetLocation(reflectionDirection)
		
		return new Ray(reflectionSource, reflectionDirection)
	}

	/**
	 * Bounce this ray in a random direction off a surface at the specified 
	 * intersect. The direction is determined by combining a perfectly reflected 
	 * ray with a randomly scattered ray in proportions determined by the 
	 * 'roughness' factor of the material at the surface
	 * 
	 * @param {SceneIntersect} intersect - The location on the surface at which  
	 *     this ray is to be bounced
	 * @param {boolean} useSecondaryRoughness - Whether to use intersect 
	 *     material's primary or secondary roughness
	 * @return {Ray}
	 */
	bounce(intersect, useSecondaryRoughness = false) {
		// Get a perfectly reflected ray
		const reflectedRay = this.reflect(intersect)
		// Get a randomly scattered ray
		const scatteredRay = intersect.scatter()
		// Combine the ray directions in proportions determined by material 
		// roughness
		const direction = Vector.interpolate(
			reflectedRay.direction,
			scatteredRay.direction,
			useSecondaryRoughness 
				? 1 - intersect.material.secondaryRoughness
				: 1 - intersect.material.roughness
		).normalise()

		return new Ray(reflectedRay.source, direction)
	}

	static fromPlainJS(ray) {
		return new Ray(
			Vector.fromPlainJS(ray.source),
			Vector.fromPlainJS(ray.direction)
		)
	}
}