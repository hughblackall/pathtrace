import Vector from './Vector.js'
import Ray from './Ray.js'
import Material from './Material.js'

/**
 * Represents the result of checking whether a ray intersects with any objects 
 * in a scene
 * 
 * @
 */
export default class SceneIntersect {
	/**
	 * Create an intersect
	 */
	constructor() {
		/**
		 * True if an intersection occurs
		 * @type {boolean}
		*/
		this.intersects = false
		
		/**
		 * The location at which the intersection occurs
		 * @type {Vector}
		 */
		this.location = null
		
		/**
		 * A normalised vector representing the direction perpendicular to the 
		 * surface the intersection occurs with
		 * @type {Vector}
		 */
		this.normal = null
		
		/**
		 * The distance of the intersection from the ray origin
		 * @type {number}
		 */
		this.distance = Number.MAX_VALUE
		
		/**
		 * The material of the object the intersection occurs with
		 * @type {Material}
		 */
		this.material = null
		
		/**
		 * Whether the intersect is with a light or not
		 * @type {boolean}
		 */
		this.light = false
	}

	/**
	 * Shift this intersect location a small amount in either the normal 
	 * direction or opposite the normal direction, so that when casting rays 
	 * from it, the rays don't intersect with the surface the intersect is on
	 * 
	 * @param {Vector} targetDirection - The direction in which to move along 
	 *     the normal
	 * @return {Vector}
	 */
	getOffsetLocation(targetDirection) {
		return targetDirection.dot(this.normal) < 0
					? this.location.subtract( this.normal.scale(1e-3) )
					: this.location.add (this.normal.scale(1e-3) )
	}

	/**
	 * Create a new ray scattered in a random direction off the surface at this
	 * intersect. The source of the new ray will be offset slightly in the 
	 * normal direction to avoid intersecting with the surface off which it is 
	 * being scattered.
	 */
	scatter() {
		// Find a random scatter target by adding the unit normal vector and a 
		// random unit vector. This produces a sphere of possible targets above 
		// the scattering surface centred at the tip of the unit normal vector.
		const scatterTarget = this.normal
			.add(Vector.randomNormalised())
			.add(this.location)
		// The direction of the new ray is the new source subtracted from the 
		// new target. The direction vector needs to be normalised as it could 
		// be of any length within the 'target' sphere.
		const scatterDirection = 
			scatterTarget.subtract(this.location).normalise()
		// Move the intersect location in the normal direction slightly so that 
		// the new ray doesn't intersect with the scattering object
		const scatterSource = this.getOffsetLocation(scatterDirection)
		
		return new Ray(scatterSource, scatterDirection)
	}
}