/**
 * Represents a colour in the RGB colour model. Colour components are 
 * represented on a scale of 0-1.
 * */
export default class Colour {
	/**
     * Create a colour
     * @param {number} r - The red component
     * @param {number} g - The green component
     * @param {number} b - The blue component
     */
	constructor(r, g, b) {
		this.r = r
		this.g = g
		this.b = b
	}

	/**
	 * Get the red component in its 8 bit representation
	 */
	get r8bit() {
		return Math.round( this.r * 255 )
	}

	/**
	 * Get the red component in its 8 bit representation
	 */
	get g8bit() {
		return Math.round( this.g * 255 )
	}

	/**
	 * Get the red component in its 8 bit representation
	 */
	get b8bit() {
		return Math.round( this.b * 255 )
	}

	/**
	 * Add a colour to this one
	 * 
	 * @param {Colour} operand - The colour to add
	 * @returns {Colour} - The result as a new colour
	 */
	add(operand) {
		return new Colour(
			this.r + operand.r,
			this.g + operand.g,
			this.b + operand.b
		)
	}

	/**
	 * Scale this colour
	 * 
	 * @param {number} factor - The factor to scale this colour by
	 * @returns {Colour}
	 */
	scale(factor) {
		return new Colour(
			this.r * factor,
			this.g * factor,
			this.b * factor
		)
	}

	/**
	 * Multiply a colour with this one
	 * 
	 * @param {Colour} operand - The colour to add
	 * @returns {Colour} - The result as a new colour
	 */
	multiply(operand) {
		return new Colour(
			this.r * operand.r,
			this.g * operand.g,
			this.b * operand.b
		)
	}
	
	/**
	 * Counteract the gamma curve of the display's output.
	 * 
	 * @param {number} gammaValue - The gamma value of the display to counteract
	 */
	gammaCorrect(gammaValue = 2.2) {
		// A typical digital display will adjust colours so that linear changes 
		// in colour values appear as linear changes in brightness to the human 
		// eye. Real world linear changes in luminosity aren't perceived as 
		// linear by the human eye. Small actual changes in brightness are 
		// perceived as larger at low brightness levels than at high brightness 
		// levels, roughly corresponding to an inverse gamma curve (x^(1/ɣ)). A 
		// path tracer is attempting to simulate real world lighting conditions 
		// and so must present changes in brightness as they would appear to the 
		// human eye in the real world by applying an inverse gamma curve 
		// adjustment. The display will then correct brightness with its gamma 
		// curve (x^ɣ) and linear changes in lighting and brightness settings 
		// will be perceived linearly in the rendered image.
		// https://learnopengl.com/Advanced-Lighting/Gamma-Correction
		return new Colour(
			Math.pow(this.r, 1/gammaValue),
			Math.pow(this.g, 1/gammaValue),
			Math.pow(this.b, 1/gammaValue)
		)
	}

	get hex() {
		return '#' 
			+ this.r8bit.toString(16).padStart(2, '0')
			+ this.g8bit.toString(16).padStart(2, '0')
			+ this.b8bit.toString(16).padStart(2, '0')
	}

	/**
	 * Find a colour that is the result of linearly interpolating between two 
	 * colours by a given fraction. This process can be thought of as 'mixing' 
	 * colours together.
	 * 
	 * @param {Vector} colour1 
	 * @param {Vector} colour2 
	 * @param {Vector} fraction
	 */
	static interpolate(colour1, colour2, fraction) {
		return colour1.scale(fraction).add(colour2.scale(1 - fraction))
	}

	static fromPlainJS(colour) {
		return new Colour(colour.r, colour.g, colour.b)
	}
}