import Vector from './Vector.js'
import Camera from './Camera.js'

/**
 * Represents an infinite plane in the x and y dimensions of 3 dimensional 
 * Euclidean space to be used as a focal plane for a camera
 */
export default class FocalPlane {
	/**
	 * Create a focal plane
	 * 
	 * @param {number} z - The z coordinate of the plane
	 */
	constructor(z) {
		this.z = z
	}

	/**
	 * Check if a camera ray intersects with the plane.
	 * 
	 * There are four possibilities: there is a single point of intersection, 
	 * the ray intersects but points away from the plane, the camera lies on the 
	 * plane, or the ray is parallel to the plane and never intersects. Due to 
	 * of field of view calculations in this path tracer the field of view must 
	 * be less than 180 degrees, and so we can safely ignore the last three
	 * possibilities.
	 * 
	 * @param {Vector} rayDirection - The ray to check for intersection
	 * @param {Camera} camera - The camera from which the ray originates
	 * @return {Vector}
	 */
	rayIntersect(rayDirection, camera) {
		
		// d is the distance from the ray source to the point of intersection 
		// with the plane
		const d = (this.z - camera.location.z) / rayDirection.z

		// Get the intersect position by adding the camera position vector and 
		// the vector from the camera to the plane
		return camera.location.add(rayDirection.scale(d))
	}

	static fromPlainJS(focalPlane) {
		return new FocalPlane(focalPlane.z)
	}
}