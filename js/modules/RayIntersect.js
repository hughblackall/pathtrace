/** Represents the result of checking whether a ray intersects with an object */
export default class RayIntersect {
	/**
	 * Create an intersect
	 * 
	 * @param {boolean} intersects - True if an intersection occurs
	 * @param {number | null} distance - The distance from the ray source at 
	 *     which the intersection occurs, provided an intersection did occur. 
	 *     Otherwise is null.
	 */
	constructor(intersects, distance) {
		this.intersects = intersects
		if (distance)
			this.distance = distance
		else
			this.distance = null
	}
}