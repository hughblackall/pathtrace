import Scene from "./Scene.js"
import RayCaster from "./RayCaster.js"
import Camera from "./Camera.js"

/** 
 * A pool of workers that can run tasks in parallel. Tasks in this case are rows 
 * of pixels of a frame to be rendered.
 * 
 * Implements a stack rather than a queue to track tasks to be executed such 
 * that the most recent task to be added to the pool is executed first. This is
 * to avoid an expensive shift operation to get new tasks when there are large 
 * numbers of tasks waiting to be executed. As a result this pool shouldn't be 
 * used when execution order of tasks is important.
 */
export default class WorkerPool {
	/**
	 * Create a worker pool
	 * 
	 * @param {number} threadCount - The number of workers in the pool
	 * @param {function} completeCallback - A function to be called when each 
	 *     task is completed
	 * @param {*} bindTo - An object to bind `this` to during execution of the 
	 *     callback
	 */
	constructor(threadCount, completeCallback, bindTo) {
		this.threadCount = threadCount
		this.completeCallback = completeCallback
		this.bindTo = bindTo
		this.workerQueue = []
		this.taskStack = []
		this.workerThreads = []
		this.readyResolve = null
	}

	/**
	 * Initialise the worker pool by creating and initialising workers 
	 * 
	 * @param {Scene} scene - The scene to be referenced for rendering by 
	 *     workers in the pool
	 * @param {Camera} camera - A camera to define the position from which
	 *     to capture the parts of scene rendered in workers
	 * @param {*} settings - Settings to pass to a RayCaster on each worker
	 */
	init(scene, camera, settings) {
		return new Promise((resolve, reject) => {
			
			this.readyResolve = resolve
			for (var i=0; i < this.threadCount; i++) {
				this.workerThreads.push(
					new WorkerThread(
						this,
						this.completeCallback.bind(this.bindTo),
						scene,
						camera,
						settings
					)
				)
			}
		})
	}

	// TODO: Document the structure of a task
	/**
	 * Add a task to the pool to be executed in parallel by a worker
	 * 
	 * @param {*} task - The task to execute
	 */
	addTask(task) {
		return new Promise((resolve, reject) => {
			task.resolve = resolve
			if (this.workerQueue.length > 0) {
				// Get a worker from the front of the worker queue
				const workerThread = this.workerQueue.shift()
				workerThread.run(task)
			} else {
				// If there are no free workers, put the task on the stack to be 
				// executed later
				this.taskStack.push(task)
			}
		})
	}

	/**
	 * Called when a worker has completed a task
	 * 
	 * @param {WorkerThread} workerThread - The worker thread that has completed 
	 *     a task
	 */
	freeWorkerThread(workerThread) {
		if (this.taskStack.length > 0) {
			// If there are tasks remaining, execute the next task
			var task = this.taskStack.pop()
			workerThread.run(task)
		} else {
			// If there are no tasks remaining, put the worker back in the 
			// worker queue
			this.workerQueue.push(workerThread)
		}
	}

	/**
	 * Called when a worker has completed initialisation
	 *
	 * @param {WorkerThread} workerThread - The worker thread that has completed
	 *     initialisation
	 */
	workerThreadReady(workerThread) {
		this.workerQueue.push(workerThread)
		let allReady = true
		for (let i=0; i < this.workerThreads.length; i++) {
			allReady = this.workerThreads[i].ready
		}
		if (allReady) {
			this.readyResolve({message: 'All worker threads ready'})
		}
	}

	/**
	 * Clean up workers once the worker pool is no longer needed
	 */
	terminate() {
		this.workerQueue.forEach(workerThread => {
			workerThread.worker.terminate()
		})
	}
}

/**
 * A worker that can execute tasks on a thread independent of other workers. 
 * Uses a web worker under the hood.
 */
class WorkerThread {
	/**
	 * Create a worker and initialise it
	 * 
	 * @param {WorkerPool} parentPool - The parent worker pool that this worker 
	 *     will belong to
	 * @param {function} completeCallback - A function that is called when this 
	 *     worker has completed a task
	 * @param {Scene} scene - The scene to be referenced for rendering
	 * @param {Camera} camera - A camera to define the position from which
	 *     to capture the parts of scene to be rendered
	 * @param {*} settings - Settings to pass to a RayCaster on the worker
	 */
	constructor(parentPool, completeCallback, scene, camera, settings) {
		this.parentPool = parentPool
		this.completeCallback = completeCallback
		this.settings = settings
		this.scene = scene
		this.camera = camera
		this.task = null
		this.worker = new Worker('js/modules/RayCaster.js', { type: 'module' })
		this.worker.addEventListener('message', this.complete.bind(this))
		this.ready = false
		this.worker.postMessage({ 
			type: 'init',
			scene: this.scene,
			camera: this.camera,
			settings: this.settings
		})
	}

	/**
	 * Run a task on this worker
	 * 
	 * @param {*} task - The task to run
	 */
	run(task) {
		this.task = task
		this.worker.postMessage({
			type: RayCaster.messageType.render,
			...task,
			resolve: undefined,
		})
	}

	/**
	 * Called by the web worker when a task is complete
	 * 
	 * @param {*} event - The event passed by the web worker on completion of a 
	 *     task
	 */
	complete(event) {
		if (event.data == 'setup-done') {
			this.ready = true
			this.parentPool.workerThreadReady(this)
		} else {
			const result = {
				...event.data,
			}
			this.task.resolve(result)
			this.completeCallback(result)
			this.task = null
			this.parentPool.freeWorkerThread(this)
		}
	}
}