/** Measures the execution time of a function */
export default class Timer {
	/**
	 * Create a timer
	 */
	constructor() {
		/**
		 * The run time of the most recent timed execution in milliseconds. Null
		 * if execution is incomplete or no executions have been started.
		 * @type {number | null}
		*/
		this.executionTime = null
	}

	/**
	 * Execute a function and log its execution time in milliseconds
	 * 
	 * @param {function} timedFunction - The function to execute and time
	 * @param {*} bindTo - An object to bind `this` to during execution of the 
	 *     timed function
	 * @param  {...any} args - Arguments to pass to the timed function
	 * @return {*} - The return value of the timed function
	 */
	time(timedFunction, bindTo, ...args) {
		this.executionTime = null

		const boundFunction = timedFunction.bind(bindTo)
		const start = performance.now()
		const result = boundFunction(...args)
		const end = performance.now()

		this.executionTime = end - start

		console.info(
			`${timedFunction.name}() execution completed in ${this.executionTime} milliseconds`
		)

		return result
	}

	/**
	 * Execute a function that returns a promise and log the time taken in 
	 * milliseconds for the promise to resolve
	 * 
	 * @param {function} timedFunction - The function to execute and time
	 * @param {*} bindTo - An object to bind `this` to during execution of the 
	 *     timed function
	 * @param  {...any} args - Arguments to pass to the timed function
	 * @return {Promise}
	 */
	timeAsync(timedFunction, bindTo, ...args) {
		this.executionTime = null

		const boundFunction = timedFunction.bind(bindTo)
		const start = performance.now()
		let end = null
		const promise = boundFunction(...args)
		promise.then(() => {
			end = performance.now()
			this.executionTime = end - start
			console.info(
				`${timedFunction.name}() execution completed in ${this.executionTime} milliseconds`
			)
		})

		return promise
	}

	executionTimeSeconds({decimalPlaces}) {
		return (this.executionTime / 1000).toFixed(decimalPlaces)
	}
}