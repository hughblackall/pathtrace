/**
 * Comprises the ray casting part of the path tracer. To be run in a web worker 
 * and will render a row of a frame in parallel.
 */

import Scene from './Scene.js'
import Camera from './Camera.js'
import Vector from './Vector.js'
import Ray from './Ray.js'
import Colour from './Colour.js'
import Light from './Light.js'

let rayCaster = null

// Listen for instructions
addEventListener('message', e => {
	// If instruction is to initialise, create a ray caster
	if (e.data.type == RayCaster.messageType.init) {
		let { scene, camera, settings } = e.data
		rayCaster = new RayCaster({
			scene: Scene.fromPlainJS(scene),
			camera: Camera.fromPlainJS(camera),
			settings
		})
		console.debug('posting message: setup-done')
		postMessage('setup-done')

	} else if (e.data.type == RayCaster.messageType.render) {
		// If instruction is to render a row, loop through each pixel in the row 
		// and cast a ray for each
		let { row, width, height } = e.data
		let rowBuffer = new Uint16Array(width * 4)

		for (let col=0; col < width; col++) {

			let ray = rayCaster.camera.pixelDirection(
				row, col, width, height
			)

			let pixel = rayCaster.castRay(Ray.fromPlainJS(ray))

			let redIndex = col * 4
			let greenIndex = redIndex + 1
			let blueIndex = redIndex + 2
			let alphaIndex = redIndex + 3

			rowBuffer[redIndex] = pixel.gammaCorrect().r8bit
			rowBuffer[greenIndex] = pixel.gammaCorrect().g8bit
			rowBuffer[blueIndex] = pixel.gammaCorrect().b8bit
			rowBuffer[alphaIndex] = 255
		}
		const transferable = rowBuffer.buffer
		postMessage({ rowBuffer: transferable, row }, [transferable])
	}
})

/** Casts rays through a scene using a path tracing algorithm */
export default class RayCaster {
	/**
	 * Create a ray caster
	 * 
	 * @param {Object} params
	 * @param {Scene} params.scene - The scene to render
	 * @param {Camera} params.camera - A camera to define the position from which 
	 *     to capture the scene
	 * @param {number} params.settings.maxDepth - The maximum number of ray 
	 *     reflections to perform
	 * @param {boolean} params.settings.bounceMasking - Whether to represent 
	 *     absorption of colours as rays bounce off objects in the scene
	 */
	constructor({ scene, camera, settings: { maxDepth, bounceMasking, allowSecondaryRoughness } }) {
		this.scene = scene
		this.camera = camera
		this.settings = {
			maxDepth,
			bounceMasking,
			allowSecondaryRoughness,
		}
		// TODO: Make the below properties useful
		this.depthCount = []
		this.intersectCount = {}
	}

	static messageType = { init: 'init', render: 'render' }

	/**
	 * Cast a ray in the given direction and return the colour that the ray 
	 * 'sees'
	 * 
	 * @param {Ray} ray - The ray to cast
	 * @param {number} depth - The bounce recursion depth to cast this ray at
	 * @param {Colour} mask - A mask that is passed down the bounce recursion 
	 *     stack to cumulatively track light absorption at each intersect
	 * @return {Colour} - The resulting colour the ray 'sees'
	 */
	castRay(ray, depth = 0, mask = new Colour(1, 1, 1)) {

		// Cast the ray and check for intersections with scene objects
		let sceneIntersect = this.scene.rayIntersect(ray)

		// If the ray intersects with a light, apply the light colour to the 
		// mask and return
		if (sceneIntersect.light) {
			this.countIntersect('light')
			return sceneIntersect.material.colour.multiply(mask)

			// If the ray intersects with an object, calculate the colour that 
			// should be returned based on the position and intensity of light 
			// sources, other objects in the scene and the material of the object
		} else if (sceneIntersect.intersects
			&& depth <= this.settings.maxDepth) {
			this.countIntersect('object')
			this.countDepth(depth)

			// Multiply the mask by the colour of the surface the ray 
			// intersects. The mask will later be combined with light intensity 
			// to determine the colour this ray returns. At a depth of 0, the 
			// mask will simply be the same as this colour, but as more rays are 
			// recursively cast the mask will get smaller. This represents 
			// accumulated absorption as the ray bounces off objects in the 
			// scene.
			if (this.settings.bounceMasking) {
				mask = mask.multiply(sceneIntersect.material.colour)
			}

			// Loop through the lights in the scene
			let lightAccumulator = new Colour(0, 0, 0)
			this.scene.sceneObjects.filter(
				(object) => object instanceof Light
			).forEach((light) => {

				// Get a random point on the light to cast a ray at
				let lightPoint = light.position
					.add(Vector.randomNormalised().scale(light.radius))

				// Get the direction and distance of this light point from the 
				// point where the ray intersects the scene object
				let lightDirection = lightPoint
					.subtract(sceneIntersect.location)
					.normalise()
				let lightDistance = lightPoint
					.subtract(sceneIntersect.location)
					.magnitude

				// Check if the intersection lies in a shadow of another object 
				// for this point on this light
				// First move the intersect location slightly in the normal 
				// direction to ensure the ray used to check for other objects  
				// in front of the light doesn't intersect with the original 
				// object itself
				let shiftedIntersect =
					sceneIntersect.getOffsetLocation(lightDirection)
				let shadowRay = new Ray(shiftedIntersect, lightDirection)
				let shadowIntersect = this.scene.rayIntersect(shadowRay)

				// If there is nothing between this light and the original 
				// object, continue adding the impact of the light to the 
				// resulting colour. Otherwise skip adding the impact of this 
				// light.
				if (shadowIntersect.light) {

					// Modulate the total light intensity for this light by 
					// looking at the light intensity, the angle of incidence of 
					// the light on the scene object (dot products are great for 
					// this), and the apparent size of the light
					let angleOfIncidenceFactor =
						shadowRay.direction.dot(sceneIntersect.normal)
					if (angleOfIncidenceFactor < 0) angleOfIncidenceFactor = 0
					let apparentSizeFactor = light.apparentSize(lightDistance)
					lightAccumulator = lightAccumulator.add(
						light.material.colour.scale(
							light.intensity
							* angleOfIncidenceFactor
							* apparentSizeFactor
						)
					)
				}
			})

			// Recursively cast a new ray from the intersection of the current 
			// ray in a random direction influenced by reflection and roughness 
			// of the intersect surface to get the colour contribution of 
			// reflections at this intersection
			const bouncedRay = ray.bounce(sceneIntersect)
			let bounceColour = this.castRay(bouncedRay, depth + 1, mask)
			if (sceneIntersect.material.hasSecondaryRoughness 
				&& this.settings.allowSecondaryRoughness) {
				// If intersect material has a secondary roughness then bounce a 
				// second ray for that roughness
				const secondaryBouncedRay = ray.bounce(sceneIntersect, true)
				const secondaryBounceColour = 
					this.castRay(secondaryBouncedRay, depth + 1, mask)
				
				// Mix the resultant colours of the primary and secondary 
				// roughness bounced rays based on the material roughness mix
				bounceColour = Colour.interpolate(
					bounceColour,
					secondaryBounceColour,
					1 - sceneIntersect.material.roughnessMix
				)
			}

			// If bounce masking is enabled then combine the mask with light 
			// intensity to determine the colour this ray returns
			// Scale the influence of lights on the surface for this intersect 
			// by the roughness of the intersect material
			if (this.settings.bounceMasking) {
					return mask.multiply(lightAccumulator)
						.scale(
							sceneIntersect.material.averageRoughness(
								this.settings.allowSecondaryRoughness
							)
						).add(bounceColour)
				
			} else {
				return sceneIntersect.material.colour
					.multiply(lightAccumulator)
					.scale(
						sceneIntersect.material.averageRoughness(
							this.settings.allowSecondaryRoughness
						)
					).add(bounceColour)
			}

			// Otherwise the ray intersects with nothing or the bounce limit has 
			// been exceeded. Stop bouncing and return the ambient light colour 
			// for this bounce.
		} else {
			this.countIntersect('nothing')
			return mask.multiply(
				this.scene.ambientColour.scale(this.scene.ambientIntensity)
			)
		}
	}

	countDepth(depth) {
		if (!this.depthCount[depth]) {
			this.depthCount[depth] = 1
		} else {
			this.depthCount[depth]++
		}
	}

	countIntersect(type) {
		if (!this.intersectCount[type]) {
			this.intersectCount[type] = 1
		} else {
			this.intersectCount[type]++
		}
	}
}