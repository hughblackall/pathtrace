import Vector from './Vector.js'
import FocalPlane from './FocalPlane.js'
import Ray from './Ray.js'

/**
 * Represents a viewpoint, facing in the negative z direction, from which to 
 * render the scene
 */
export default class Camera {
	/**
	 * Create a camera
	 * 
	 * @param {Object} params
	 * @param {Vector} params.location - The location of the camera in 3 dimensional 
	 *     Euclidean space
	 * @param {number} params.fov - The field of view of the camera in radians
	 * @param {number} params.focalDepth - The distance of the focal plane from 
	 *     the camera
	 * @param {number} params.depthOfFieldStrength - The strength of the depth 
	 *     of field effect
	 * @param {boolean} params.depthOfField - Whether depth of field is 
	 *     enabled for this camera
	 * @param {boolean} params.antiAlias - Whether anti-aliasing is 
	 *     enabled for this camera
	 */
	constructor({
		location, fov, focalDepth, depthOfFieldStrength, depthOfField = false,
		antiAlias = false
	}) {
		this.location = location
		this.fov = fov
		this.focalPlane = new FocalPlane(location.z - focalDepth)
		this.depthOfFieldStrength = depthOfFieldStrength
		this.depthOfField = depthOfField
		this.antiAlias = antiAlias
	}

	/**
	 * Get a ray pointing at a pixel from the camera location
	 * 
	 * @param {number} row - The row of the pixel
	 * @param {number} col - The column of the pixel
	 * @param {number} width - The width of the screen in pixels
	 * @param {number} height - The height of the screen in pixels
	 * @return {Ray}
	 */
	pixelDirection(row, col, width, height) {

		// Let center of the screen be in the -z direction from the camera
		// The x component is multiplied by the screen aspect ratio. This is 
		// necessary as we're using the same fov for both the horizontal and 
		// vertical dimensions, otherwise our screen would be square in world 
		// space.
		// The -1s shift the screen to center around the z axis
		// Because our framebuffer rows are indexed with smaller numbers at the 
		// top of the screen, which is at odds with how spatial coordinate 
		// systems are often defined (positive y is usually up), we need to flip 
		// the image by applying a - to our y component 
		// If using anti-aliasing, shift the sample location away from the 
		// centre of the pixel by a random amount
		const subPixelOffset = this.antiAlias 
			? {x: Math.random(), y: Math.random()} 
			: {x: 0.5, y: 0.5}

		const x =  (2 * (col + subPixelOffset.x) / width  - 1) * Math.tan(this.fov/2) * width/height
		const y = -(2 * (row + subPixelOffset.y) / height - 1) * Math.tan(this.fov/2)
		const pixelDirection = new Vector(x, y, -1).normalise()
		
		if (this.depthOfField) {
			return this.depthOfFieldOffset(pixelDirection)
		}
		return new Ray(this.location, pixelDirection)
	}

	/**
	 * Randomly offset the camera to get a new pixel direction pointing at the 
	 * same location on the focal plane, in order to create a depth of field 
	 * effect
	 * 
	 * @param {Vector} pixelDirection - Normalised direction of a pixel
	 */
	depthOfFieldOffset(pixelDirection) {

		// Find the position at which the original ray intersects the focal 
		// plane
		const focalIntersect = 
			this.focalPlane.rayIntersect(pixelDirection, this)
		
		// Offset the camera's position slightly in a random direction on the xy 
		// plane
		// Get a vector pointing in a random direction
		let offsetVector = Vector.randomNormalised()
		// Confine it to the xy plane
		offsetVector.z = 0
		// Normalise it again, as setting the z component to 0 altered its 
		// magnitude, and scale it by a random number so that the offset is a 
		// random distance up to 1 as opposed to always being exactly 1
		offsetVector = offsetVector.normalise().scale(Math.random())
		// Scale the final offset by the depth of field level
		const newCameraLocation = 
			this.location.add(offsetVector.scale(this.depthOfFieldStrength))

		// Find the new pixel direction
		let newPixelDirection = 
			focalIntersect.subtract(newCameraLocation).normalise()

		return new Ray(newCameraLocation, newPixelDirection)
	}

	get focalDepth() {
		return this.location.z - this.focalPlane.z
	}

	set focalDepth(focalDepth) {
		this.focalPlane.z = this.location.z - focalDepth
	}

	static fromPlainJS(camera) {
		let newCamera = new Camera({
			location: Vector.fromPlainJS(camera.location),
			fov: camera.fov,
			depthOfFieldStrength: camera.depthOfFieldStrength,
			depthOfField: camera.depthOfField,
			antiAlias: camera.antiAlias
		})
		newCamera.focalPlane = FocalPlane.fromPlainJS(camera.focalPlane)
		return newCamera
	}
}