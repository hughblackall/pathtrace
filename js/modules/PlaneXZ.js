import RayIntersect from './RayIntersect.js'
import Ray from './Ray.js'
import SceneObject from './SceneObject.js'
import Vector from './Vector.js'
import Material from './Material.js'

/**
 * Represents an infinite plane in the x and z dimensions of 3 dimensional 
 * Euclidean space 
 * 
 * @extends SceneObject
 */
export default class PlaneXZ extends SceneObject {
	/**
	 * Create an infinite xz dimension plane
	 * 
	 * @param {number} y - The y coordinate of the plane
	 * @param {Material} material - The material the plane is made of
	 */
	constructor(y, material) {
		super(material)
		this.y = y
		this.type = PlaneXZ.type
	}

	static type = 'plane-xz'

	/**
	 * Check if a ray intersects with the plane.
	 * 
	 * There are four possibilities: there is a single point of intersection, 
	 * the ray intersects but points away from the plane, the camera lies on the 
	 * plane, or the ray is parallel to the plane and never intersects. Due to 
	 * the nature of a path tracer generating random directions for rays, we can 
	 * safely ignore the last possibility.
	 * 
	 * @param {Ray} ray - The ray to check for intersection
	 * @return {RayIntersect}
	 */
	rayIntersect(ray) {
		
		// d is the distance from the ray source to the point of intersection 
		// with the plane
		const d = (this.y - ray.source.y) / ray.direction.y

		// If d is positive the ray intersects the plane
		if (d > 0) return new RayIntersect(true, d)

		// If d is negative or zero, the ray does not intersect the plane
		return new RayIntersect(false)
	}

	/** 
	 * Find the normal vector at a given location on the surface of the plane
	 * 
	 * @override
	 * @param {Vector} location - The location on the plane (not used for xz 
	 *     planes)
	 * @param {Ray} ray - The ray with which the plane intersects
	 * @return {Vector} - Normalised normal vector
	 */
	findNormal(location, ray) {
		return ray.source.y > this.y
			? new Vector(0, 1, 0)
			: new Vector(0, -1, 0)
	}

	static fromPlainJS(planeXZ) {
		return new PlaneXZ(planeXZ.y, Material.fromPlainJS(planeXZ.material))
	}
}