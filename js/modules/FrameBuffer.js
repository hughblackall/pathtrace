import Colour from './Colour.js'

/**
 * Stores pixel data in the 8 bit RGB colour space whilst a frame is rendered
 */
export default class FrameBuffer {
	/**
	 * Create a frame buffer and initialise all RGB pixel values to 0
	 * 
	 * @param {number} width - The width of the frame in pixels
	 * @param {number} height - The height of the frame in pixels
	 */
	constructor(width, height) {
		this.width = width
		this.height = height

		/**
		 * Array that stores pixel data as a one-dimensional array in RGBA 
		 * order, with integer values between 0 and 255
		 * @type {[number]}
		 */
		this.buffer = Array(width * height * 4).fill(0)
	}

	/** 
	 * Takes an RGB colour and sets a pixel in the frame buffer to that colour
	 * 
	 * @param {Colour} colour - The colour to set the pixel to
	 * @param {number} row - The row of the pixel to set
	 * @param {number} col - The column of the pixel to set
	 */
	setPixel(colour, row, col) {
		let redIndex   = row * this.width * 4 + col * 4
		let greenIndex = redIndex + 1
		let blueIndex  = redIndex + 2
		let alphaIndex = redIndex + 3
		
		this.buffer[redIndex]   = colour.r8bit
		this.buffer[greenIndex] = colour.g8bit
		this.buffer[blueIndex]  = colour.b8bit
		this.buffer[alphaIndex] = 255
	}

	/**
	 * Add another frame buffer to this one. Individually adds corresponding 
	 * pixels in each buffer.
	 * 
	 * @param {FrameBuffer} operand - The frame buffer to add to this one
	 */
	add(operand) {
		if (this.width != operand.width || this.height != operand.height) {
			throw new Error('Frame buffer sizes do not match')
		}

		let result = new FrameBuffer(this.width, this.height)
		for (let i=0; i < this.buffer.length; i++) {
			result.buffer[i] = this.buffer[i] + operand.buffer[i]
		}

		return result
	}

	/**
	 * Divide each pixel in the frame buffer by a constant
	 * 
	 * @param {number} constant 
	 */
	divideByConstant(constant) {

		let result = new FrameBuffer(this.width, this.height)
		for (let i=0; i < this.buffer.length; i++) {
			result.buffer[i] = this.buffer[i] / constant
		}

		return result
	}

	insertRow(arrayBuffer, row) {
		this.buffer.splice(
			row * this.width * 4,
			this.width * 4,
			...Array.from(new Uint16Array(arrayBuffer))
		)
	}
}