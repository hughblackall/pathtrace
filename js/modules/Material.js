import Colour from './Colour.js'

/** Represents the material of objects in a scene */
export default class Material {
	/**
	 * Create a material
	 * 
	 * @param {Object} params
	 * @param {Colour} params.colour - The colour of the material
	 * @param {number | null} params.roughness - The roughness of the material. Null 
	 *     if material doesn't need a roughness e.g. for lights.
	 * @param {number | null} params.secondaryRoughness - A secondary roughness of the 
	 *     material. Can be used to simulate a material with a partially 
	 *     transparent surface layer. Null if material has primary roughness 
	 *     only.
	 * @param {number} params.roughnessMix - A number between 0 and 1 that determines 
	 *     how much of each roughness value to use. 0 will use primary roughness 
	 *     only, 1 will use secondary roughness only.
	 */
	constructor({ colour, roughness, secondaryRoughness = null, roughnessMix = 0 }) {
		this.colour = colour
		this.roughness = roughness
		this.secondaryRoughness = secondaryRoughness
		this.roughnessMix = roughnessMix
	}

	/**
	 * Whether this material has a secondary roughness
	 * 
	 * @return {boolean}
	 */
	get hasSecondaryRoughness() {
		return this.secondaryRoughness != null && this.roughnessMix > 0
	}

	/**
	 * Get the average roughness of this material based on its primary roughness 
	 * and the mix between the two
	 * 
	 * @param {boolean} allowSecondaryRoughness - Whether secondary roughnesses 
	 *     are enabled in the path tracer
	 * @return {number}
	 */
	averageRoughness(allowSecondaryRoughness) {
		if (this.hasSecondaryRoughness && allowSecondaryRoughness) {
			return this.roughness * (1 - this.roughnessMix)
					+ this.secondaryRoughness * (this.roughnessMix)
		} else {
			return this.roughness
		}
	}

	static fromPlainJS(material) {
		return new Material({
			colour: Colour.fromPlainJS(material.colour),
			roughness: material.roughness,
			secondaryRoughness: material.secondaryRoughness,
			roughnessMix: material.roughnessMix,
		})
	}
}