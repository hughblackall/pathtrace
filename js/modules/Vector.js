/** Represents a 3 dimensional vector in Euclidean space */
export default class Vector {
	/**
     * Create a vector
     * @param {number} x - The x component
     * @param {number} y - The y component
     * @param {number} z - The z component
     */
	constructor(x, y, z) {
		this.x = x
		this.y = y
		this.z = z
	}

	/**
	 * Add a three dimensional vector to this one
	 * 
	 * @param {Vector} operand - The vector to add
	 * @returns {Vector} - The result as a new vector
	 */
	add(operand) {
		return new Vector(
			this.x + operand.x,
			this.y + operand.y,
			this.z + operand.z
		)
	}

	/**
	 * Subtract a three dimensional vector from this one
	 * 
	 * @param {Vector} operand - The vector to subtract
	 * @returns {Vector} - The result as a new vector
	 */
	subtract(operand) {
		return new Vector(
			this.x - operand.x,
			this.y - operand.y,
			this.z - operand.z
		)
	}

	/**
	 * Find the dot product of this vector and another three dimensional vector
	 * 
	 * @param {Vector} operand - The vector to compute the dot product with
	 * @returns {number}
	 */
	dot(operand) {
		return this.x * operand.x
			 + this.y * operand.y
			 + this.z * operand.z
	}

	/**
	 * Scale this vector
	 * 
	 * @param {number} scalar - The scalar to scale this vector by
	 * @returns {Vector}
	 */
	scale(scalar) {
		return new Vector(
			this.x * scalar,
			this.y * scalar,
			this.z * scalar
		)
	}

	/**
	 * Find the magnitude of this vector
	 * 
	 * @returns {number} - The magnitude of this vector
	 */
	get magnitude() {
		return Math.sqrt(
			this.x * this.x
		  + this.y * this.y
		  + this.z * this.z
		)
	}

	/**
	 * Scale this vector so that its magnitude is 1, also known as normalisation
	 * 
	 * @returns {Vector} - The normalised vector
	 */
	normalise() {
		if (this.magnitude > 0)
		   return this.scale(1 / this.magnitude)
		else
			return this
	}

	/**
	 * Negate this vector
	 * 
	 * @returns {Vector} - The negated vector
	 */
	negate() {
		return new Vector(0, 0, 0).subtract(this)
	}

	/**
	 * Find a reflection unit vector if this vector is an incident vector using
	 *   I - 2(I⋅N)N
	 * where I is the unit incident vector (pointing towards the reflecting 
	 * surface) and N is the unit normal vector
	 * 
	 * @param {Vector} normal - The unit normal vector at the point a ray 
	 *     strikes a surface
	 * @return {Vector} - A unit vector pointing in the direction of reflection
	 */
	reflect(normal) {
		// This vector serves as the incident vector pointing in the direction a 
		// ray is travelling when it strikes a surface
		const incident = this.normalise()
		// Find the projection of the incident vector on the normal. We scale 
		// this by 2 in the next step to make the base of an isosceles triangle, 
		// with the reflection vector at the 'pointy end' and the normal along 
		// the base
		let projectionIncidentOnNormal = normal.scale( incident.dot(normal) )
		// The incident vector points towards the reflecting surface and so the 
		// above dot product will be negative, therefore we need to subtract it 
		// from the incident to give the reflection vector
		return incident.subtract( projectionIncidentOnNormal.scale(2) )
	}

	/** Get a normalised vector that points in a random direction */
	static randomNormalised() {
		let result = null
		// Discard any generated vector that has a magnitude greater than 1 to 
		// avoid biasing results towards the corners of the 3D generation space
		while (!result || result.magnitude > 1) {
			result = new Vector(
				Math.random() * 2 - 1,
				Math.random() * 2 - 1,
				Math.random() * 2 - 1
			)
		}
		return result.normalise()
	}

	/**
	 * Find a vector that is the result of linearly interpolating between two 
	 * vectors by a given fraction. This process can be thought of as 'mixing' 
	 * vectors together.
	 * 
	 * @param {Vector} vector1 
	 * @param {Vector} vector2 
	 * @param {Vector} fraction
	 */
	static interpolate(vector1, vector2, fraction) {
		return vector1.scale(fraction).add(vector2.scale(1 - fraction))
	}

	static fromPlainJS(vector) {
		return new Vector(vector.x, vector.y, vector.z)
	}
}