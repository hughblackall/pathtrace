import Vector from './modules/Vector.js'
import Light from './modules/Light.js'
import Sphere from './modules/Sphere.js'
import PathTracer from './modules/PathTracer.js'
import Scene from './modules/Scene.js'
import Timer from './modules/Timer.js'
import PlaneXZ from './modules/PlaneXZ.js'
import Material from './modules/Material.js'
import Colour from './modules/Colour.js'
import Camera from './modules/Camera.js'

// Define the default scene
let scene = new Scene(
	[
		new Sphere(
			new Vector(-8.5, -2, -42),
			3,
			new Material({ colour: new Colour(1, 1, 1), roughness: 1 })
		),
		new Sphere(
			new Vector(0, -2, -42),
			3,
			new Material({ colour: new Colour(1, 1, 1), roughness: 0, secondaryRoughness: 1, roughnessMix: 0.95})
		),
		new Sphere(
			new Vector(8.5, -2, -42),
			3,
			new Material({ colour: new Colour(1, 1, 1), roughness: 0})
		),
		new Sphere(
			new Vector(-8.5, -3.5, -36.5),
			1.5,
			new Material({ colour: new Colour(1, 0, 0), roughness: 1 })
		),
		new Sphere(
			new Vector(0, -3.5, -36.5),
			1.5,
			new Material({ colour: new Colour(0, 0, 1), roughness: 1 })
		),
		new Sphere(
			new Vector(8.5, -3.5, -36.5),
			1.5,
			new Material({ colour: new Colour(0, 1, 0), roughness: 1 })
		),

		new Sphere(
			new Vector(-4.25, -4.5, -25),
			0.5,
			new Material({ colour: new Colour(1, 1, 1), roughness: 1 })
		),
		new Sphere(
			new Vector(4.25, -4.5, -26),
			0.5,
			new Material({ colour: new Colour(1, 1, 1), roughness: 1 })
		),
		new Sphere(
			new Vector(2, -4.75, -29),
			0.25,
			new Material({ colour: new Colour(0.8, 0.5, 1), roughness: 1 })
		),
		new Sphere(
			new Vector(-3.5, -4.75, -31),
			0.25,
			new Material({ colour: new Colour(1, 1, 0.5), roughness: 1 })
		),
		new Sphere(
			new Vector(-1.75, -4.8, -28.5),
			0.2,
			new Material({ colour: new Colour(1, 0.55, 0.4), roughness: 1 })
		),

		new PlaneXZ(
			-5,
			new Material({ colour: new Colour(1, 1, 1).scale(1), roughness: 1 })
		),
		new Light(new Vector(-50, 40, -40), 18, 4, new Colour(1, 1, 1).scale(3)),
	],
	new Colour(0.01, 0.01, 0.01), 1
)

// Add display properties to scene objects
scene.sceneObjects.map((object) => {
	object.isExpanded = false
})
scene.sceneObjects[0].isExpanded = true

const camera = new Camera({
	location: new Vector(0, 0, 0),
	fov: 0.4,
	focalDepth: 40,
	depthOfField: true,
	depthOfFieldStrength: 0.3,
	antiAlias: true
})

const pathTracer = new PathTracer({
	scene,
	camera,
	settings: {
		maxDepth: 10,
		bounceMasking: true,
		threadCount: 4,
		allowSecondaryRoughness: true,
	},
})

var vm = new Vue({
	el: '#vue-app',
	data: {
		pathTracer,
		camera,
		scene,
		renderActive: false,
		frameCount: 0,
		frameLimit: 200,
		renderTime: null,
		resolution: { width: 1024, height: 576 },
		selectedTab: 'scene-settings',
		selectingNewSceneObject: false,
	},
	methods: {
		renderClick() {
			vm.renderActive = true
			vm.frameCount = 0
			vm.renderTime = null

			let timer = new Timer()
			let promise = timer.timeAsync(
				pathTracer.render,
				pathTracer,
				{ frameLimit: vm.frameLimit, frameReadyCallback: vm.frameReady }
			)

			promise.then(() => {
				vm.renderActive = false
				vm.renderTime =
					timer.executionTimeSeconds({ decimalPlaces: 2 })

				console.debug('Depth counter:', pathTracer.depthCount)
				console.debug('Intersect counter:', pathTracer.intersectCount)
				const intersectTotal = pathTracer.intersectCount['object'] + pathTracer.intersectCount['light'] + pathTracer.intersectCount['nothing']
				console.debug('Intersect total:', intersectTotal)
				pathTracer.intersectCount['object'] = pathTracer.intersectCount['object'] / intersectTotal
				pathTracer.intersectCount['light'] = pathTracer.intersectCount['light'] / intersectTotal
				pathTracer.intersectCount['nothing'] = pathTracer.intersectCount['nothing'] / intersectTotal
				console.debug('Intersect fraction:', pathTracer.intersectCount)
			})
		},
		
		/** Called by the path tracer when a new frame has been rendered and averaged */
		frameReady({ frameBuffer, frameCount, frameLimit }) {
			vm.frameCount = frameCount

			console.debug(`Displaying frame ${frameCount} of ${frameLimit}`)
			vm.displayFrameBuffer(frameBuffer)
		},

		displayFrameBuffer(frameBuffer) {
			
			if(ctx) {
				const imageData = ctx.createImageData(1024, 576)
				for (let i = 0; i < imageData.data.length; i++) {
					imageData.data[i] = frameBuffer.buffer[i]
				}
				ctx.putImageData(imageData, 0, 0)
			}
		},

		addSceneObject(type) {
			let newObject = null
			const position = new Vector(0, 0, -13)
			const material = new Material({
				colour: new Colour(Math.random(), Math.random(), Math.random()),
				roughness: Math.random(),
			})
			
			switch (type) {
				case Sphere.type:
					newObject = new Sphere(position, 1, material)
					break
				case PlaneXZ.type:
					newObject = new PlaneXZ(-5, material)
					break
				case Light.type:
					newObject = new Light(
						position, 1, 5, new Colour(1, 1, 0.5).scale(6)
					)
					break
			}

			newObject.isExpanded = true

			scene.sceneObjects.unshift(newObject)
		},

		duplicateSceneObject(index) {
			const source = scene.sceneObjects[index]
			let duplicate = JSON.parse(JSON.stringify(source))

			console.log(duplicate)

			switch (source.type) {
				case Sphere.type:
					duplicate = Sphere.fromPlainJS(duplicate)
					break
				case PlaneXZ.type:
					duplicate = PlaneXZ.fromPlainJS(duplicate)
					break
				case Light.type:
					duplicate = Light.fromPlainJS(duplicate)
					break
			}

			duplicate.isExpanded = true

			scene.sceneObjects.splice(index + 1, 0, duplicate)
		},

		deleteSceneObject(index) {
			console.log(index)
			scene.sceneObjects.splice(index, 1)
		},

		sceneObjectTitle(type) {
			switch (type) {
				case Sphere.type:
					return 'Sphere'
				case PlaneXZ.type:
					return 'Plane XZ'
				case Light.type:
					return 'Light'
			}
		},

		sceneObjectIcon(type) {
			switch (type) {
				case Sphere.type:
					return 'mdi-circle'
				case PlaneXZ.type:
					return 'mdi-checkbox-blank'
				case Light.type:
					return 'mdi-lightbulb-on'
			}
		},
	}
})

const canvas = document.getElementById('screen-sink')
let ctx = null
if (canvas.getContext) {
	ctx = canvas.getContext('2d')
}